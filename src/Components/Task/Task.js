import React from "react";
import "./Task.css";

const Task = ({ task, deleted }) => {
  return (
    <div className="Task">
      <p>{task.text}</p>
      <button onClick={deleted}>X</button>
    </div>
  );
};

export default Task;
