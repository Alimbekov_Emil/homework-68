import React from "react";
import "./TodoForm.css";

const TodoForm = ({ add, value, change }) => {
  return (
    <div className="TodoForm">
      <input type="text" value={value} className="Field" onChange={change} />
      <button onClick={add}>Add</button>
    </div>
  );
};

export default TodoForm;
