import React from "react";
import { NavLink } from "react-router-dom";
import "./Layout.css";

const Layout = (props) => {
  return (
    <div className="layout">
      <header className="Header">
        <NavLink exact to="/">
          Counter
        </NavLink>
        <NavLink exact to="/todo">
          Todo
        </NavLink>
      </header>
      {props.children}
    </div>
  );
};

export default Layout;
