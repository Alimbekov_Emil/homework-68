import React from "react";
import "./Modal.css";
import Backdrop from "../Backdrop/Backdrop";
import Spinner from "../Spinner/Spinner";

const Modal = (props) => {
  return (
    <>
      <Backdrop show={props.show} />
      <div
        className="Modal"
        style={{
          transform: props.show ? "translateY(0)" : "translateY(-100vh)",
          opacity: props.show ? "1" : "0",
        }}
      >
        {props.show ? <Spinner /> : null}Loading...
      </div>
    </>
  );
};

export default Modal;
