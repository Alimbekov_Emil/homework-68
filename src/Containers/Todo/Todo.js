import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Task from "../../Components/Task/Task";
import TodoForm from "../../Components/TodoFrom/TodoForm";
import Modal from "../../Components/UI/Modal/Modal";
import {
  AddNewTask,
  ADD_TASK,
  CHANGE,
  deleteTask,
  fetchTodo,
} from "../../store/actions";
import "./Todo.css";

const Todo = () => {
  const dispatch = useDispatch();
  const tasks = useSelector((state) => state.tasks);
  const inputValue = useSelector((state) => state.input);
  const loading = useSelector((state) => state.loading);

  useEffect(() => {
    dispatch(fetchTodo());
  }, [dispatch]);

  const addTask = () => {
    if (inputValue !== "") {
      dispatch(AddNewTask({ text: inputValue }));
      dispatch({ type: ADD_TASK });
    }
  };

  const inputChange = (e) => dispatch({ type: CHANGE, value: e.target.value });
  const deleteClick = (id) => dispatch(deleteTask(id));

  return (
    <div className="Todo">
      <TodoForm
        add={addTask}
        value={inputValue}
        change={(e) => inputChange(e)}
      />
      <Modal show={!!loading} />

      {tasks.map((task) => {
        return (
          <Task
            key={task.id}
            task={task}
            deleted={() => deleteClick(task.id)}
          />
        );
      })}
    </div>
  );
};

export default Todo;
