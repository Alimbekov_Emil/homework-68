import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Modal from "../../Components/UI/Modal/Modal";
import {
  add,
  decrement,
  fetchCounter,
  increment,
  subtract,
} from "../../store/actions";
import "./Counter.css";

const Counter = () => {
  const dispath = useDispatch();

  const counter = useSelector((state) => state.counter);
  const loading = useSelector((state) => state.loading);

  useEffect(() => {
    dispath(fetchCounter());
  }, [dispath]);

  const increaseCounter = () => dispath(increment());
  const decreaseCounter = () => dispath(decrement());
  const plusCounter = () => dispath(add(5));
  const minusCounter = () => dispath(subtract(5));

  return (
    <div className="Counter">
      <Modal show={!!loading} />
      <h1>{counter}</h1>
      <button onClick={increaseCounter}>Increase</button>
      <button onClick={decreaseCounter}>Descrease</button>
      <button onClick={plusCounter}>Increase by 5</button>
      <button onClick={minusCounter}>Descrease by 5</button>
    </div>
  );
};

export default Counter;
