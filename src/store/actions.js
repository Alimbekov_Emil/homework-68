import axiosUrl from "../axios-url";

export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";
export const ADD = "ADD";
export const SUBTRACT = "SUBTRACT";

export const ADD_TASK = "ADD_TASK";
export const CHANGE = "CHANGE";

export const FETCH_COUNTER_REQUEST = "FETCH_COUNTER_REQUEST";
export const FETCH_COUNTER_SUCCESS = "FETCH_COUNTER_SUCCESS";
export const FETCH_COUNTER_FAILURE = "FETCH_COUNTER_FAILURE";

export const FETCH_TODO_REQUEST = "FETCH_TODO_REQUEST";
export const FETCH_TODO_SUCCESS = "FETCH_TODO_SUCCESS";
export const FETCH_TODO_FAILURE = "FETCH_TODO_FAILURE";

export const increment = () => {
  return (dispatch) => {
    dispatch({ type: INCREMENT });
    dispatch(fetchCounterPut());
  };
};

export const decrement = () => {
  return (dispatch) => {
    dispatch({ type: DECREMENT });
    dispatch(fetchCounterPut());
  };
};
export const add = (value) => {
  return (dispatch) => {
    dispatch({ type: ADD, value });
    dispatch(fetchCounterPut());
  };
};

export const subtract = (value) => {
  return (dispatch) => {
    dispatch({ type: SUBTRACT, value });
    dispatch(fetchCounterPut());
  };
};

export const fetchCounterRequest = () => ({ type: FETCH_COUNTER_REQUEST });
export const fetchCounterSuccess = (counter) => ({
  type: FETCH_COUNTER_SUCCESS,
  counter,
});
export const fetchCounterFailure = () => ({ type: FETCH_COUNTER_FAILURE });

export const fetchCounter = () => {
  return async (dispatch) => {
    dispatch(fetchCounterRequest());
    try {
      const response = await axiosUrl.get("counter.json");
      dispatch(fetchCounterSuccess(response.data));
    } catch (e) {
      dispatch(fetchCounterFailure());
    }
  };
};

export const fetchCounterPut = () => {
  return async (dispatch, getState) => {
    const counter = getState().counter;
    try {
      await axiosUrl.put("counter.json", counter);
    } catch (e) {
      dispatch(fetchCounterFailure());
    }
  };
};

export const fetchTodoRequest = () => ({ type: FETCH_TODO_REQUEST });
export const fetchTodoSuccess = (tasks) => ({
  type: FETCH_TODO_SUCCESS,
  tasks,
});
export const fetchTodoFailure = () => ({ type: FETCH_TODO_FAILURE });

export const fetchTodo = () => {
  return async (dispatch) => {
    dispatch(fetchTodoRequest());
    try {
      const response = await axiosUrl.get("todo.json");
      const array = [];
      if (response.data !== null) {
        const responseKey = Object.keys(response.data);
        responseKey.forEach((id) => {
          const idObj = response.data[id];
          for (const key in idObj) {
            if (idObj.hasOwnProperty(key)) {
              const text = idObj[key];
              array.push({ text, id });
            }
          }
        });
      }
      dispatch(fetchTodoSuccess(array));
    } catch (e) {
      dispatch(fetchTodoFailure());
    }
  };
};

export const deleteTask = (id) => {
  return async (dispatch) => {
    await axiosUrl.delete("todo/" + id + ".json");
    dispatch(fetchTodo());
  };
};

export const AddNewTask = (task) => {
  return async (dispatch) => {
    await axiosUrl.post("todo.json", task);
    dispatch(fetchTodo());
  };
};
