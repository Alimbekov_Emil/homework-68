import {
  ADD,
  ADD_TASK,
  CHANGE,
  DECREMENT,
  FETCH_COUNTER_FAILURE,
  FETCH_COUNTER_REQUEST,
  FETCH_COUNTER_SUCCESS,
  FETCH_TODO_FAILURE,
  FETCH_TODO_REQUEST,
  FETCH_TODO_SUCCESS,
  INCREMENT,
  SUBTRACT,
} from "./actions";

const initialState = {
  counter: 0,
  tasks: [],
  input: "",
  loading: true,
  error: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return { ...state, counter: state.counter + 1 };
    case DECREMENT:
      return { ...state, counter: state.counter - 1 };
    case ADD:
      return { ...state, counter: state.counter + action.value };
    case SUBTRACT:
      return { ...state, counter: state.counter - action.value };
    case FETCH_COUNTER_REQUEST:
      return { ...state, loading: true };
    case FETCH_COUNTER_SUCCESS:
      return { ...state, loading: false, counter: action.counter };
    case FETCH_COUNTER_FAILURE:
      return { ...state, loading: false, error: true };
    case ADD_TASK:
      return { ...state, input: "" };
    case CHANGE:
      return { ...state, input: action.value };
    case FETCH_TODO_REQUEST:
      return { ...state, loading: true };
    case FETCH_TODO_SUCCESS:
      return { ...state, loading: false, tasks: action.tasks };
    case FETCH_TODO_FAILURE:
      return { ...state, loading: false, error: true };

    default:
      return state;
  }
};
export default reducer;
