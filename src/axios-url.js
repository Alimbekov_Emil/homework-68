import axios from "axios";

const axiosUrl = axios.create({
  baseURL: "https://alimbekov-class-work-63-default-rtdb.firebaseio.com/",
});

export default axiosUrl;
