import React from "react";
import { BrowserRouter, NavLink, Route, Switch } from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import Counter from "./Containers/Counter/Counter";
import Todo from "./Containers/Todo/Todo";

const App = () => (
  <Layout>
    <Switch>
      <Route path="/" exact component={Counter} />
      <Route path="/todo" exact component={Todo} />
    </Switch>
  </Layout>
);

export default App;
